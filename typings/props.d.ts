import { IPropsInput, IPropsOutput } from "./types.js";
export declare function props(promisesObj: IPropsInput | Promise<IPropsInput>): Promise<IPropsOutput>;
