export declare function reduce(iterable: any[] | Promise<any[]>, cb: (acc: any, input: any) => any, initial: any): Promise<any>;
