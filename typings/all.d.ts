export declare function all(promises: Iterable<any> | Promise<Iterable<any>>): Promise<Array<any>>;
